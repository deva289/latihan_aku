<?php

class bangunruang{
    public $luaspermukaanlimas;
    public $volumelimas;
    public $luaspermukaankubus;
    public $volumekubus;
}

class limas extends bangunruang{

    public function luaspermukaanlimas($la,$lsl,$½,$ka,$ts){
        echo "<h4>Luas Permukaan Limas</h4>";
        echo "luas alas :".$la." cm<br>";
        echo "Luas Selumbung Limas :".$lsl." cm<br>";
        echo "1/4 :".$½." cm<br>";
        echo "keliling alas :".$ka." cm<br>";
        echo "tinggi segitiga :".$ts." cm<br>";

        $this->luaspermukaanlimas = $la+$lsl ;
        $this->luaspermukaanlimas2 = $ka+$½+$ts ;

        echo "Luas 1 : ".$this->luaspermukaanlimas." cm<br>";
        echo "Luas 2 : ".$this->luaspermukaanlimas2." cm";
    }
    public function volumelimas($½,$sa,$t){
        echo "<h4>Volume Limas</h4>";
        echo "½ :".$½." cm<br>";
        echo "Sisi Atas :".$sa." cm<br>";
        echo "Tinggi :".$t." cm<br>";

        $this->volumelimas = $½*$sa*$sa*$t;

        echo "Volume Limas : " .$this->volumelimas." cm³";
    }
    public function luaspermukaankubus($a,$s1,$s2){
        echo "<h4>Luas Permukaan kubus</h4>";
        echo "a :".$a." cm<br>";
        echo "Sisi 1 :".$s1." cm<br>";
        echo "Sisi atas :".$s2." cm<br>";

        $this->luaspermukaankubus = $a*$s1*$s2;

        echo "Luas Permukaan Kubus :".$this->luaspermukaankubus." cm<br>";
    }
    public function volumekubus(){
        echo "<h4>Volume Kubus</h4>";
        echo "";
    }
}

$cetak = new limas;
echo "<h2><center><u>Bangun Ruang</u></center></h2>";
echo "<h3>Menghitung Luas Permukaan Limas & Volume Limas</h3>";
$cetak->luaspermukaanlimas(5,3,7,9,4);
$cetak->volumelimas(3,6,8);
echo "<hr>";
echo "<br>";
echo "<h2><center><u>Luas Permukaan Kubus</u></center></h2>";
echo "<h3>Menghitung Luas Permukaan Kubus & Volume Kubus</h3>";
$cetak->luaspermukaankubus(6,3,3);

?>