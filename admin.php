<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Login</title>
    <style>
      body{
        background-image: url("img/bg.jpg");
        background-repeat: no-repeat;
        background-size:cover
      }
    </style>
  </head>
  <body>
  <nav class="navbar navbar-dark" style="background-color: #B8860B">
  <div class="container">
    <a class="navbar-brand" href="index.php">Via Hotel</a>
    <button style="background-color: white; border-radius: 0.5rem;"><a style="text-decoration: none; color: black; font-size: 0.8rem;" href="admin.php">ADMIN</a></button>
  </div>
</nav>
<center>
<div class="card" style="margin-top: 5rem; width: 30rem; padding-bottom: 1rem;">
<center>
    <h3 style="margin-top: 1rem;">Login Admin</h3>
    <form action="index_admin.php" method="post">
<table style="margin-top: 1rem;">
  <tr>
    <td><h4>Username</h4></td>
    <td><h4><input type="text" name="username"></h4></td>
  </tr>
  <tr>
    <td><h4>Password</h4></td>
    <td><h4><input type="password" name="password"></h4></td>
  </tr>
  <tr>
        <td></td>
        <td><input class="btn btn-primary" type="submit" value="Login"></td>
    </tr>
</table>
</form>
</center>
</div>
</center>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html> 