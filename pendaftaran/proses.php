<?php
include '../database.php';
$pendaftaran1 = new Pendaftaran();

if(isset($_POST['save'])){
    $aksi = $_POST['aksi'];
    $id   = @$_POST['id'];
    $kode_pendaftaran = $_POST['kode_pendaftaran'];
    $nama = $_POST['nama'];
    $tanggal_lahir = $_POST['tanggal_lahir'];
    $tempat_lahir = $_POST['tempat_lahir'];
    $jenis_kelamin = $_POST['jenis_kelamin'];
    $agama = $_POST['agama'];
    $jurusan = $_POST['jurusan'];

    if ($aksi == "create"){
        $pendaftaran1->create($kode_pendaftaran, $nama, $tanggal_lahir, $tempat_lahir, $jenis_kelamin, $agama, $jurusan);
        header("location:index2.php");
    }
    else if ($aksi == "update") {
        $pendaftaran1->update($id,$kode_pendaftaran, $nama, $tanggal_lahir, $tempat_lahir, $jenis_kelamin, $agama, $jurusan);
        header("location:index2.php");
    }
    else if ($aksi == "delete"){
        $pendaftaran1->delete($id);
        header("location:index2.php");
    }
}
?>