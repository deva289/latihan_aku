<?php

class laptop{
    private $pemilik = "mulki";
    private $merk = "jeruk";

    public function __constructor(){
        echo "ini berasal dari construct laptop";
    }

    public function hidupkan_laptop(){
        return " Hidupkan Laptop $this->merk punya $this->pemilik";
    }

    public function __destructor(){
        echo " ini berasal dari destructor laptop";
    }
}

$laptop_mulki = new laptop();
echo "<br/>";
echo $laptop_mulki ->hidupkan_laptop();
echo "<br/>";

unset ($laptop_mulki);
echo "<br/>";
echo "object telah dihapus";
?>