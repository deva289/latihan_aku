<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Home</title>
    <style>
      body{
        background-image: url("img/bg.jpg");
        background-repeat: no-repeat;
        background-size:cover
      }
    </style>
  </head>
  <body>
  <nav class="navbar navbar-dark" style="background-color: #B8860B;">
  <div class="container">
    <a class="navbar-brand" href="index.php">Via Hotel</a>
  </div>
</nav>
<?php
$nama_pemesan = $_POST['nama_pemesan'];
$lama_menginap = $_POST['lama_menginap'];
$nama_kamar = $_POST['nama_kamar'];

if($nama_kamar == "Kamar Rakyat"){
    $harga_kamar = 200000;
}
elseif($nama_kamar == "Kamar Pejabat"){
    $harga_kamar = 500000;
}
elseif($nama_kamar == "Kamar Kelas Kakap"){
    $harga_kamar = 1000000;
}
$total_harga = $harga_kamar * $lama_menginap;
?>
<center>
<div class="card" style="width: 40rem; margin-top: 2rem; border-radius: 1rem; padding-bottom: 1rem; background-color: #B8860B;">
      <h3 style="margin-top: 1rem;">STRUK PEMBAYARAN</h3>
      <center>
          <form action="struk.php" method="post">
      <table style="margin-top: 1rem;">
          <tr>
              <td><h5>Nama Pemesan</h5></td>
              <td><h5>&nbsp;: <input type="text" name="nama_pemesan" value=" <?= $nama_pemesan ?>" readonly></h5></td>
          </tr>
          <tr>
              <td><h5>Jenis Kamar</h5></td>
              <td><h5>&nbsp;: <input type="text" name="nama_kamar" value=" <?= $nama_kamar ?>" readonly></h5></td>
          </tr>
          <tr>
              <td><h5>Harga Kamar</h5></td>
              <td><h5>&nbsp;: <input type="text" name="harga_kamar" value="<?= $harga_kamar ?>" readonly></h5></td>
          </tr>
          <tr>
              <td><h5>Lama Menginap</h5></td>
              <td><h5>&nbsp;: <input type="text" name="lama_menginap" value=" <?= $lama_menginap ?>" readonly></h5></td>
          </tr>
          <tr>
              <td><h5>Total Harga</h5></td>
              <td><h5>&nbsp;: Rp. <input style="width: 12.5rem;" type="text" name="total_harga" value="<?= $total_harga ?>" readonly></h5></td>
          </tr>
          <tr>
              <td><h5>Uang Masuk</h5></td>
              <td><h5>&nbsp;: Rp. <input style="width: 12.5rem;" type="text" name="uang_masuk" value=""></h5></td>
          </tr>
          <tr>
              <td></td>
              <td>&nbsp;&nbsp;<input class="btn btn-primary" type="submit" value="Bayar"></td>
        </tr>
      </table>
    </form>
    </center>
</div>
</center>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>