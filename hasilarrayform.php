<?php
if(isset($_POST['simpan'])){
    $nis = $_POST['nis'];
    $nama = $_POST['nama'];
    $kelas = $_POST['kelas'];
    $Nilai1 = $_POST['Indonesia'];
    $Nilai2 = $_POST['Inggris'];
    $Nilai3 = $_POST['Matematika'];
    $Nilai4 = $_POST['Produktif'];
}
?>
<html>
    <body>
        <center>
            <h2>Data Siswa Kelas XII RPL</h2>
            <table border="1">
                <tr>
                    <th>NIS</th>
                    <th>Nama</th>
                    <th>Kelas</th>
                    <th>Nilai B.Indonesia</th>
                    <th>Nilai B.Inggris</th>
                    <th>Nilai Matematika</th>
                    <th>Nilai Produktif</th>
                    <th>Rata-rata</th>
                    <th>Grade</th>
                    <?php
                    for($a=0; $a < count($nama); $a++){
                    ?>
                </tr>
                <tr>
                    <td><?php echo $nis[$a]; ?></td>
                    <td><?php echo $nama[$a]; ?></td>
                    <td><?php echo $kelas[$a]; ?></td>
                    <td><?php echo $Nilai1[$a]; ?></td>
                    <td><?php echo $Nilai2[$a]; ?></td>
                    <td><?php echo $Nilai3[$a]; ?></td>
                    <td><?php echo $Nilai4[$a]; ?></td>
                    <td>
                        <?php $rata_rata = ($Nilai1[$a] + $Nilai2[$a] + $Nilai3[$a] + $Nilai4[$a]) / 4; 
                        echo $rata_rata;
                        ?>
                    </td>
                   <?php
                    if($rata_rata >= 90 && $rata_rata <= 100) {
                        echo "<td>A</td>";
                    }
                    elseif ($rata_rata >= 80 && $rata_rata < 90){
                        echo "<td>B</td>";
                    }
                    elseif ($rata_rata >= 70 && $rata_rata < 80) {
                        echo "<td>C</td>";
                    }
                    elseif ($rata_rata >= 60 && $rata_rata < 70) {
                        echo "<td>D</td>";
                    }
                    elseif ($rata_rata >= 0 && $rata_rata < 60) {
                        echo "<td>E</td>";
                    }
                    else {
                        echo "<td>TIDAK LULUS</td>";
                    }
                }
                    ?>
            </table>
        </center>
    </body>
</html>