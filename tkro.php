<html>
    <body>
        <center>
        <h1>TKRO SMK ASSALAAM BANDUNG</h1>
        <br>
            <h2>Pengertian Teknik Kendaraan Ringan Otomotif</h2>
<h3>Teknik Kendaraan Ringan adalah ilmu yang mempelajari tentang alat-alat transportasi darat yang menggunakan mesin, terutama mobil yang mulai berkembang sebagai cabang ilmu seiring dengan diciptakannya mesin mobil.
Perkembangan teknologi yang semakin cepat ini juga seiring dengan perkembangan kendaraan yang semakin canggih. Salah satu kendaraan yang semakin maju adalah mobil. Mobil merupakan alat transportasi yang cukup komplek dengan memiliki beberapa komponen. Komponen-komponen ini mempunyai peran masing-masing yang tergolong dalam puluhan sistem dan subsistem.</h3>
<br>
<h3>Maka dari itu, jurusan teknik kendaraan ringan hadir untuk mengembangkan ilmu mengenai teknologi kendaraan. Teknik kendaraan ringan temasuk juga dalam ilmu teknik otomotif namun lebih spesifik. Ilmu ini bertujuan untuk melaksanakan perawatan dan perbaikan komponen-komponen mobil secara mandiri, merawat dan memperbaiki mobil sesuai dengan standar yang ditentukan oleh pabrik.
Selain itu juga mempelajari mengenai merawat dan memperbaiki mobil pada bengkel atau perusahaan di mana nantinya tempat ia bekerja, serta menciptakan lapangan kerja baru bagi dirinya dan orang lain.</h3>
        
        <h2>Tujuan Ilmu Teknik Kendaraan Ringan</h2>
        
        <h3>Tujuan Kompetensi Keahlian Teknik Kendaraan Ringan secara umum mengacu pada isi Undang Undang Sistem Pendidikan Nasional (UU SPN) Pasal 3 tentang Tujuan Pendidikan Nasional dan penjelasan Pasal 15 yang menyebutkan bahwa pendidikan kejuruan merupakan pendidikan menengah yang mempersiapkan peserta didik terutama untuk bekerja dalam bidang tertentu
Jadi, tujuan dari mempelajari ilmu ini adalah untuk mencetak pelajar yang mampu mengaplikasikan ilmu dasar teknik kendaraan tingan, memperdalam pengetahuan, dan mengasah keterampilan di bidang tersebut.
Kompotensi keberhasilan dari ilmu teknik kendaraan ringan yaitu :
Memiliki karakter, mampu berkompetisi dan mengembangkan sikap professional dalam kompetensi keahlian Teknik Kendaraan Ringan
Menciptakan lapangan kerja sendiri atau berwirausaha dalam bidang kompetensi keahlian Teknik Kendaraan Ringan
Melanjutkan ke jenjang pendidikan yang lebih tinggi sesuai kompetensi yang dimiliki</h3>
</center>
    </body>
</html>