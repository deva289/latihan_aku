<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Admin</title>
    <style>
      body{
        background-image: url("img/bg.jpg");
        background-repeat: no-repeat;
        background-size:cover
      }
    </style>
  </head>
  <body>
    <?php
$username = $_POST['username'];
$password = $_POST['password'];
    ?>

  <nav class="navbar navbar-dark" style="background-color: #B8860B">
  <div class="container">
    <a class="navbar-brand" href="index.php">Via Hotel</a>
    <a class="nav-link" href="index.php" style="margin-left: -51rem; color: white;">Riwayat Pemesanan</a>
    <button style="background-color: white; border-radius: 0.5rem;"><a style="text-decoration: none; color: black; font-size: 0.8rem;" href="admin.php">Logout</a></button>
  </div>
</nav>
<?php if($username == 'kelompok3' && $password == 'kelompok3') : ?>
<div class="" style="margin-top: 5rem;">
<center>
<table>
  <tr>
    <td>
      <div class="card" style="width: 19rem; margin-right: 2rem; background-color: #B8860B; border-radius: 1rem; border: 1px solid white">
      <img src="img/Standard.jpg" class="card-img-top" alt="..." style="border-radius: 1rem;">
      <div class="card-body">
      <h5 class="card-title">Kamar Rakyat</h5>
      <p class="card-text">Price : Rp.200.000/Malam</p>
      <a href="#" class="btn btn-primary">Ubah</a>
      <a href="#" class="btn btn-danger">Hapus</a>
    </div>
    </td>
    <td>
    <div class="card" style="width: 19rem; margin-right: 2rem; background-color: #B8860B; border-radius: 1rem; border: 1px solid white">
      <img src="img/Deluxe.jpg" class="card-img-top" alt="..." style="border-radius: 1rem;">
      <div class="card-body">
      <h5 class="card-title">Kamar Pejabat</h5>
      <p class="card-text">Price : Rp.500.000/Malam</p>
      <a href="#" class="btn btn-primary">Ubah</a>
      <a href="#" class="btn btn-danger">Hapus</a>
    </div>
    </td>
    <td>
    <div class="card" style="width: 19rem; margin-right: 2rem; background-color: #B8860B; border-radius: 1rem; border: 1px solid white">
      <img src="img/President.jpg" class="card-img-top" alt="..." style="border-radius: 1rem;">
      <div class="card-body">
      <h5 class="card-title">Kamar Kelas Kakap <br></h5>
      <p class="card-text">Price : Rp.1.000.000/Malam</p>
      <a href="#" class="btn btn-primary">Ubah</a>
      <a href="#" class="btn btn-danger">Hapus</a>
    </div>
    </td>
  </tr>
</table>
</center>
</div>
</div>
</div>

<?php else : ?>
  <script>
          alert('Username atau Password anda salah!');
          window.location='admin.php'
      </script>
<?php endif ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>