<?php
include '../database.php';
$pembayaran1 = new Pembayaran();

if(isset($_POST['save'])){
    $aksi = $_POST['aksi'];
    $id   = @$_POST['id'];
    $kode_pembayaran = $_POST['kode_pembayaran'];
    $nama = $_POST['nama'];
    $tanggal_pembayaran = $_POST['tanggal_pembayaran'];
    $uang_pendaftaran = $_POST['uang_pendaftaran'];
    $uang_seragam = $_POST['uang_seragam'];
    $uang_kegiatan = $_POST['uang_kegiatan'];

    if ($aksi == "create"){
        $pembayaran1->create($kode_pembayaran, $nama, $tanggal_pembayaran, $uang_pendaftaran, $uang_seragam, $uang_kegiatan);
        header("location:index3.php");
    }
    else if ($aksi == "update") {
        $pembayaran1->update($id, $kode_pembayaran, $nama, $tanggal_pembayaran, $uang_pendaftaran, $uang_seragam, $uang_kegiatan);
        header("location:index3.php");
    }
    else if ($aksi == "delete"){
        $pembayaran1->delete($id);
        header("location:index3edit.php");
    }
}
?>